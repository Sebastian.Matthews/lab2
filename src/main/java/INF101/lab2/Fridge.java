package INF101.lab2;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    int maxSize = 20;
    ArrayList<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();

    public int totalSize() {
        return maxSize;
    }

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return fridgeItems.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if(fridgeItems.size()<maxSize){
            fridgeItems.add(item);        
            return true;    
        }     
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if(fridgeItems.contains(item)){
            fridgeItems.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        fridgeItems.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> badfood = new ArrayList<>();
        // TODO Auto-generated method stub
        for (FridgeItem fridgeItem : fridgeItems) {
            if(fridgeItem.hasExpired()){
                badfood.add(fridgeItem);
            }
        }
        for (FridgeItem fridgeItem : badfood) {
            fridgeItems.remove(fridgeItem);
        }
        return badfood;
    }
}
